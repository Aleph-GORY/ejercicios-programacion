#include <stdio.h>

int main()
{
    double lado1Triang, lado2Triang, lado3Triang, perimTriang;
    char mensajeErrorLados[] = "Los tres lados deben ser números Reales positivos\n";
    char mensajeErrorDesig[] = "Los lados proporcionados no forman un triángulo\n";

    printf("Escribe el tamaño del primer lado del triángulo escaleno: ");
    scanf("%lf", &lado1Triang);
    printf("Escribe el tamaño del segundo lado del triángulo escaleno: ");
    scanf("%lf", &lado2Triang);
    printf("Escribe el tamaño del tercer lado del triángulo escaleno: ");
    scanf("%lf", &lado3Triang);

    if (0 < lado1Triang && 0 < lado2Triang && 0 < lado3Triang) {
        if ((lado1Triang<lado2Triang+lado3Triang) && (lado2Triang<lado3Triang+lado1Triang) && (lado3Triang<lado1Triang+lado2Triang)){
            perimTriang = lado1Triang + lado2Triang + lado3Triang;
            printf("El perímetro del triángulo escaleno es: %lf\n", perimTriang);
        }
        else {
            printf("%s", mensajeErrorDesig);
        }
    }
    else {
        printf("%s", mensajeErrorLados);
    }
}