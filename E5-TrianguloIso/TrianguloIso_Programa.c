#include <stdio.h>

int main()
{
    double lado1Triang, lado2Triang, perimTriang;
    char mensajeErrorLados[] = "Ambos lados deben ser números Reales positivos\n";
    char mensajeErrorDesig[] = "Los lados proporcionados no forman un triángulo\n";

    printf("Escribe el tamaño del lado distinto del triángulo isósceles: ");
    scanf("%lf", &lado1Triang);
    printf("Escribe el tamaño del lado repetido del triángulo isósceles: ");
    scanf("%lf", &lado2Triang);

    if (0 < lado1Triang && 0 < lado2Triang) {
        if (lado1Triang < 2*lado2Triang){
            perimTriang = lado1Triang + 2*lado2Triang;
            printf("El perímetro del triángulo isósceles es: %lf\n", perimTriang);
        }
        else {
            printf("%s", mensajeErrorDesig);
        }
    }
    else {
        printf("%s", mensajeErrorLados);
    }
}