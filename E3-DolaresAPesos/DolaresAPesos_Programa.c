#include <stdio.h>

#define CAMBIO_DOLAR_PESO 21.98

int main()
{
    double dolares, pesos;

    printf("Ingresa la cantidad de Dólares:");
    scanf("%lf", &dolares);
    pesos = dolares*CAMBIO_DOLAR_PESO;
    printf("%.2lf\n", pesos);
}