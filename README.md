# Introducción a la Programación
Bienvenidos a mi repo de los ejercicios relacionados al curso de introducción a la programación, en la beca de desarrollo de software de la DGTIC.
## Descripción
En este curso se trabajaron los temas de desarrollo de programas y su codificación en pseudocódigo y diagramas de flujo.
## Autor
Cruz Hinojosa Armando Benjamín
### Contacto
aleph\_g@ciencias.unam.mx
## Instructor
Daniel Barajas González
