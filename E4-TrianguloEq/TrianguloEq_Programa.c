#include <stdio.h>

int main()
{
    double ladoTriang, perimTriang;
    char mensajeError[] = "El lado debe ser un número Real positivo\n";

    printf("Escribe el tamaño del lado del triángulo: ");
    scanf("%lf", &ladoTriang);
    if (0 < ladoTriang) {
        perimTriang = 3*ladoTriang;
        printf("El perímetro del triángulo equilátero es: %lf\n", perimTriang);
    }
    else {
        printf("%s", mensajeError);
    }
}