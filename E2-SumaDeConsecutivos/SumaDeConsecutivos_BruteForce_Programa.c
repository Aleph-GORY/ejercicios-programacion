#include <stdio.h>

int main()
{
    int cotaSuperior, sumaAcum = 0;
    char mensajeError[] = "La cota superior debe ser un número entre 1 y 50\n";

    printf("Ingresa la cota superior:");
    scanf("%d", &cotaSuperior);

    if (1 <= cotaSuperior && cotaSuperior <= 50) {
        for(int i = 1; i <= cotaSuperior; i++) {
            sumaAcum += i;
        }
        printf("%d\n", sumaAcum);
    }
    else {
        printf("%s", mensajeError);
    }
}